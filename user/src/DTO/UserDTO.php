<?php

namespace App\DTO;

use OpenApi\Attributes\Property;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use OpenApi\Attributes as OA;


class UserDTO {
    public function __construct(

    #[Groups(["read","update", "create"])]
    public readonly string|array|null $id = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $name = null,

    #[Assert\NotBlank(message: "roles is required", groups: ["create"])]
    #[Groups(["read","create","update"])]
    public readonly array|null $roles = null,
    
    ) {}
}