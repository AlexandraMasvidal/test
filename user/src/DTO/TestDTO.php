<?php

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class TestDTO
{

    public function __construct(

        #[Groups(["read","create","update"])]
        public readonly ?string $id = null,
        
        #[Groups(["read","create","update"])]
        public readonly ?string $prop1 = null,
       
                    #[Assert\NotBlank(message: "prop2 is required", groups: ["create"])]
        #[Groups(["read","create","update"])]
        public readonly ?float $prop2 = null,
       
        #[Assert\NotBlank(message: "prop3 is required", groups: ["create"])]
        #[Groups(["read","create","update"])]
        public readonly ?\DateTime $prop3 = null,
       
                #[Assert\NotBlank(message: "prop4 is required", groups: ["create"])]
        #[Groups(["read","create","update"])]
        public readonly ?bool $prop4 = null,
       
        
                ) {}
}