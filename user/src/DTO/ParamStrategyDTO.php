<?php

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class ParamStrategyDTO
{

    public function __construct(

        // strings strategies

        #[Groups(["read","create","update"])]
        public readonly ?string $like = null,

        #[Groups(["read","create","update"])]
        public readonly ?string $ilike = null,

        #[Groups(["read","create","update"])]
        public readonly ?string $start = null,

        #[Groups(["read","create","update"])]
        public readonly ?string $istart = null,

        #[Groups(["read","create","update"])]
        public readonly ?string $end = null,

        #[Groups(["read","create","update"])]
        public readonly ?string $iend = null,

        // numbers & dates strategies

        #[Groups(["read","create","update"])]
        public readonly mixed $gt = null,

        #[Groups(["read","create","update"])]
        public readonly mixed $gte = null,

        #[Groups(["read","create","update"])]
        public readonly mixed $lt = null,

        #[Groups(["read","create","update"])]
        public readonly mixed $lte = null,


                  

    ) {}
}