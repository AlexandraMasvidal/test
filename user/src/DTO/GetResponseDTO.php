<?php

namespace App\DTO;
use OpenApi\Attributes as OA;


class GetResponseDTO
{
    public function __construct(
        string $classname,
    ) {}

    #[OA\Property(
        description: 'Error message if any.',
        type: 'string|bool',
        nullable: false
    )]
    public string|bool $error;

    #[OA\Property(
        description: 'Data response.',
        nullable: false,
        oneOf: [new Schema]
    )]
    public $data;

}