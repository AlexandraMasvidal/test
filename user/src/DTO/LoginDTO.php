<?php

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class LoginDTO
{

    public function __construct(

    #[Assert\NotBlank(message: "username is required", groups: ["create"])]
    #[Groups(["read","create","update"])]
    public readonly string $username,

    #[Groups(["read","create","update"])]
    public readonly ?string $password = null, 

                  

    ) {}
}