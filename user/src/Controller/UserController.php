<?php

namespace App\Controller;

use App\Document\User;
use App\DTO\GetResponseDTO;
use App\DTO\UserDTO;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    public function __construct(
        private DocumentManager $dm,
        private ValidatorInterface $validator,
        private SerializerInterface $serializer,
    ){}


    #[Route("/api/user", methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: "description",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: User::class, groups: ['read'])),
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string|bool',
                ),
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'user id',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'sort',
        description: 'array to sort query by fields name with asc or desc values. - '.
                     'Example sort[date]=asc&sort[name]=desc will sort the results by date and by name',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'nested',
        description: 'array to search into nested objects fields.',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    public function findBy ( #[MapQueryString] ?UserDTO $userdto ) :JsonResponse
    {
        // if no params, return the result of findAll
        if(empty($userdto)) $result = $this->dm->getRepository(User::class)->findAll();

        // if params, return the result of findby
        else {
            // converts class to array and keep only not null values
            $userdto = array_filter(json_decode(json_encode($userdto), true), fn($value) => $value !== null);
            $result = $this->dm->getRepository(User::class)->findBy($userdto);
        }

        // normalize results
        $normalizedResult = $this->serializer->normalize($result, null, ["groups" => ['read']]);

        // return result
        return new JsonResponse([
            "data" => $normalizedResult,
            "error" => false
        ], Response::HTTP_OK);
    }





    #[Route("/api/user/{id}", methods: ['PUT'])]
    #[OA\Response(
        response: 200,
        description: 'Returns the user updated',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    ref: new Model(type: User::class, groups: ['read']),
                    type: 'object'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'user id',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    public function update ( #[MapRequestPayload] UserDTO $userdto, $id ) :JsonResponse
    {
        $documentToUpdate = $this->dm->find(User::class, $id);

        if(empty($documentToUpdate)) {
            return new JsonResponse([
                "data" => false,
                "error" => "no test matching with id " . $id . " in database"
            ], Response::HTTP_OK);
        }


        $userdto = array_filter(json_decode(json_encode($userdto), true), fn($value) => $value !== null);
        $this->dm->getHydratorFactory()->hydrate($documentToUpdate, $userdto);
        $this->dm->persist($documentToUpdate);
        $this->dm->flush();



        // return result

        return new JsonResponse([
            "data" => $this->serializer->normalize($documentToUpdate, null, ['groups' => ['read']] ),
            "error" => false
        ], Response::HTTP_OK);

    }


    #[Route("/api/user", methods: ['POST'])]
    #[OA\Response(
        response: 200,
        description: "Returns an object with 'data' key (object | false) that provides the new document created and 'error' key (string | false)",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    ref: new Model(type: User::class, groups: ['read']),
                    type: 'object'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    public function create( #[MapRequestPayload] user $user ): JsonResponse
    {
        $errors = $this->validator->validate($user, null, ['create']);

        if(count($errors) > 0) {
            $errorsString = implode(', ', array_map(fn($error) => $error->getMessage(), iterator_to_array($errors)));
            return new JsonResponse([
                "data" => false,
                "error" => $errorsString
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->dm->persist($user);
        $this->dm->flush();

        $user = $this->serializer->normalize($user, null, ['groups' => ['read']] );

        return new JsonResponse([
            "data" => $user,
            "error" => false
        ], Response::HTTP_OK);

    }

    #[Route("/api/user/{id}", methods: ['DELETE'])]
    #[OA\Response(
        response: 200,
        description: "Returns an object with 'data' key (string | false) that confirm the deletion and 'error' key (string | false)",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'string'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'user id',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    public function delete ( $id ) :JsonResponse
    {
        $document = $this->dm->find(User::class, $id);

        if(empty($document)) {
            return new JsonResponse([
                "data" => false,
                "error" => "no test matching with this id in database"
            ], Response::HTTP_BAD_REQUEST);
        }
        else {
            $this->dm->remove($document);
            $this->dm->flush();
        }
        return new JsonResponse([
            "data" => "document removed successfully",
            "error" => false
        ], Response::HTTP_OK);
    }
}