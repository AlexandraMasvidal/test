<?php

namespace App\Enum;

enum QueryParameters : string
{
    case lt = "lt";

    case lte = "lte";

    case gt = "gt";

    case gte = "gte";

    case start = "start";

    case istart = "istart";

    case end = "end";

    case iend = "iend";

    case like = "like";

    case ilike = "ilike";

    public static function getAll() : array
    {
        return [
            QueryParameters::lt->value,
            QueryParameters::lte->value,
            QueryParameters::gt->value,
            QueryParameters::gte->value,
            QueryParameters::start->value,
            QueryParameters::istart->value,
            QueryParameters::end->value,
            QueryParameters::iend->value,
            QueryParameters::like->value,
            QueryParameters::ilike->value,
        ];
    }

    public static function getNumbersComparators() : array{
        return [
            QueryParameters::lt->value,
            QueryParameters::lte->value,
            QueryParameters::gt->value,
            QueryParameters::gte->value,
        ];
    }

    public static function getStringsComparators() : array{
        return [
            QueryParameters::start->value,
            QueryParameters::istart->value,
            QueryParameters::end->value,
            QueryParameters::iend->value,
            QueryParameters::like->value,
            QueryParameters::ilike->value,
        ];
    }

    public static function getCaseSensitives() : array
    {
        return [
            QueryParameters::iend->value,
            QueryParameters::istart->value,
            QueryParameters::ilike->value,
        ];
    }

    public static function containsCaseInsensistiveComparator( array $array ) : bool
    {
        foreach($array as $item){
            if(in_array($item, QueryParameters::getCaseSensitives())){
                return true;
            }
        }
        return false;
    }


}
