<?php

namespace App\Service;

use Symfony\Component\Routing\RequestContext;

class HostIpAddressService
{
    public function __construct(private RequestContext $routerRequestContext)
    {}

    public function getHostIpAddress()
    {
        return $this->routerRequestContext->getHost();
    }

}