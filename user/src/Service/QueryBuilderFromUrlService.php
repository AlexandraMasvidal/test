<?php

namespace App\Service;

use App\Enum\QueryParameters;
use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\BSON\Regex;
use ReflectionClass;


class QueryBuilderFromUrlService
{

    public function __construct(
        private DocumentManager $dm,
    )
    {}

    public function getResult($className, $documentDTO, $queryDTO)
    {

        // associative array with "paramName" => "paramType"
        $classPropertiesAndTypes = $this->getPropertiesWithTypesFromClass($className);

        // if no params -> findall
        if(empty($documentDTO) && empty($queryDTO)) $result = $this->dm->getRepository($className)->findAll();

        // if one id provided -> find
        else if($documentDTO->id && !is_array($documentDTO->id)) $result = $this->dm->find($className, $documentDTO->id);

        // create custom query
        else {
            // init query
            $query = $this->dm->createQueryBuilder($className);

            // get array from std class
            $documentDTO = array_filter(json_decode(json_encode($documentDTO), true), fn($value) => $value !== null);
            $documentDTOKeys = array_keys($documentDTO);

            // handle every param key/value
            foreach ($documentDTO as $paramName => $paramValues) {
                $keys = array_filter(array_keys($paramValues), fn($val) => gettype($val) === "string");

                // find in list
                if(empty($keys)) {
                    $values = array_values($paramValues);
                    $query = $query->field($paramName)->in($values);
                }

                else {
                    // if key not allowed, return an error
                    $diff = array_filter(array_diff($keys, QueryParameters::getAll()), function($key) {
                        return gettype($key) === "string";
                    });

                    // if invalid key(s), return an error
                    if(!empty($diff)) return ["error" => 'params(s) ' . implode(', ', $diff) . " are not allowed"];

                    // get compaparator type
                    $comparatorNames = array_keys($paramValues);
                    $isNumberComparator = count(array_diff($comparatorNames, QueryParameters::getNumbersComparators())) === 0;
                    $isStringComparators = count(array_diff($comparatorNames, QueryParameters::getStringsComparators())) === 0;

                    // update query for number/date comparators
                    if($isNumberComparator) {
                        foreach($paramValues as $comparatorName => $comparatorValue) {
                            $query = $query->field($paramName)->$comparatorName($comparatorValue);
                        }
                    }

                    // update query for string comparators
                    if($isStringComparators) $query = $this->getStringQuery($paramName, $query, $paramValues);
                }

            }

            // handle strategy params
            if(!empty($queryDTO)) {

                // search in nested objects
                if(!empty($queryDTO->nested)) {
                    foreach ($queryDTO->nested as $key => $value) {
                        $query = $query->field($key)->equals($value);
                    }
                }

                if(!empty($queryDTO->sort)){
                    $sort = json_decode(json_encode($queryDTO->sort), true);
                    $sort = array_map(fn($value) => strtolower($value), $sort);
                    $sort = array_filter($sort, function($value, $key) use ($documentDTOKeys) {
                        return in_array($key, $documentDTOKeys) && ($value === "asc" || $value === "desc");
                    },ARRAY_FILTER_USE_BOTH);

                    foreach ($sort as $key => $value) {
                        $query = $query->sort($key, $value);
                    }
                }
            }

            $result = $query->getQuery()->toArray();
        }

        return $result;
    }


    /**
     * @param $paramName
     * @param $query
     * @param $params
     * @return mixed
     */
    private function getStringQuery($paramName, $query, $params) {

        // get string comparators
        $keys = array_keys($params);
        $pattern = '';

        if(in_array(QueryParameters::like->value, $keys)) {
            $pattern = $params[QueryParameters::like->value];
        }
        else {
            if(in_array(QueryParameters::start->value, $keys)) {
                $pattern .= '^'.$params[QueryParameters::start->value].'.*';
            }
            if(in_array(QueryParameters::istart->value, $keys)) {
                $pattern .= '^'.$params[QueryParameters::istart->value].'.*';
            }
            if(in_array(QueryParameters::end->value, $keys)) {
                $pattern .= $params[QueryParameters::end->value].'$';
            }
            if(in_array(QueryParameters::iend->value, $keys)) {
                $pattern .= $params[QueryParameters::iend->value].'$';
            }
        }
        $option = QueryParameters::containsCaseInsensistiveComparator($keys) ? 'i' : '';

        $regex = new Regex($pattern, $option);

        return $query->field($paramName)->equals($regex);
    }


    public function getPropertiesWithTypesFromClass ($classname)
    {
        $reflectionClass = new ReflectionClass($classname);
        $properties = $reflectionClass->getProperties();
        $types = [];

        foreach ($properties as $property) {
            $types[$property->getName()] = $property->getType()->getName();
        }

        return $types;
    }


}