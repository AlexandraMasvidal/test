<?php

namespace App\Service;

use App\DTO\LoginDTO;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ExternalLoginService
{
    public function __construct(
        private HttpClientInterface $client,
    )
    {
    }

    private array $externalsServices = [
        "kw",
        "autre"
    ];

    public function isLoggedOnExternalService( string $serviceName, LoginDTO $loginDTO )
    {
        if(!in_array($serviceName, $this->externalsServices)) return ["error" => "INVALID_SERVICE_NAME", "data" => false];
        $method = "isLoggedOn".ucfirst($serviceName);
        $credentials = json_decode(json_encode($loginDTO), true);
        return $this->$method($credentials);
    }

    public function isLoggedOnKw($credentials)
    {
        try {
            // Set the site ID for KW France API requests
            $credentials["siteId"] = 6;

            // Send a POST request to the KW France API to attempt to log in with the provided credentials
            $response = $this->client->request('POST', 'https://api.kwfrance.com/api/login_check', [
                'json' => $credentials,
            ]);
            return $response->getStatusCode() === 200;

        }
        catch (\Exception $e) {
            // If an error occurs while logging in with the external service, return an error response with a 500 Internal Server Error status code
            return [
                "error" => $e->getMessage(),
                "data" => false
            ];
        }
    }

}