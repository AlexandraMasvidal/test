<?php

namespace App\Service;

use App\Document\User;
use DateInterval;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class TokenService
{
    public function __construct(
        private JWTTokenManagerInterface $JWTManager,
        private SerializerInterface $serializer
    )
    {
    }

    /**
     * Creates a new JWT token with optionnal refresh token (with default expiration date of 1 year)
     * @param User $user
     * @param array $payload
     * @param string $intervalCode
     * @param bool $isRefreshToken
     * @return array
     * @throws \Exception
     */
    public function createNewTokenWithPayload(User $user, array $payload=[], string $intervalCode = 'P1Y', bool $isRefreshToken = true) : array
    {
        $expirationDate = new \DateTime();
        $expirationDate->add(new DateInterval($intervalCode));
        $payload['exp'] = $expirationDate->getTimestamp();

        if ($isRefreshToken) $refreshToken = $this->JWTManager->createFromPayload($user, $payload);
        else $refreshToken = false;

        $token = $this->JWTManager->create($user);

        return [
            "token" => $token,
            "refreshToken" => $refreshToken,
            "user" => $this->serializer->normalize($user, null, ["groups" => ['read']])
        ];
    }
}