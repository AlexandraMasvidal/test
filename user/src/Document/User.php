<?php
namespace App\Document;


use OpenApi\Attributes\Property;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
#[Document]
class User {


    #[Id]
    #[Groups(["read","update", "create"])]
    private string $id;

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $name; 

    #[Assert\NotBlank(message: "roles is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Property(type: "array[]")]
    #[Field(type: "collection" )]
    private array $roles; 


    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return User
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }


    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }


}