<?php
namespace App\Document;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
#[Document]

class Test {


    #[Id]
    #[Groups(["read","update", "create"])]
    private string $id;

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $prop1; 

    #[Assert\NotBlank(message: "prop2 is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "float" )]
    private float $prop2; 

    #[Assert\NotBlank(message: "prop3 is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "date" )]
    private \DateTime $prop3; 

    #[Assert\NotBlank(message: "prop4 is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "bool" )]
    private bool $prop4; 


    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getProp1(): ?string
    {
        return $this->prop1;
    }

    /**
     * @param string|null $prop1
     * @return Test
     */
    public function setProp1(?string $prop1): self
    {
        $this->prop1 = $prop1;
        return $this;
    }

    /**
     * @return float
     */
    public function getProp2(): float
    {
        return $this->prop2;
    }

    /**
     * @param float $prop2
     * @return Test
     */
    public function setProp2(float $prop2): self
    {
        $this->prop2 = $prop2;
        return $this;
    }


    /**
     * @return \DateTime
     */
    public function getProp3(): \DateTime
    {
        return $this->prop3;
    }

    /**
     * @param \DateTime $prop3
     * @return Test
     */
    public function setProp3(\DateTime $prop3): self
    {
        $this->prop3 = $prop3;
        return $this;
    }

    /**
     * @return bool
     */
    public function getProp4(): bool
    {
        return $this->prop4;
    }

    /**
     * @param bool $prop4
     * @return Test
     */
    public function setProp4(bool $prop4): self
    {
        $this->prop4 = $prop4;
        return $this;
    }


}