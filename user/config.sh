### --------- CONFIG BUNDLES --------- ##

# get variables from .env file
source .env

##### NELMIO

# Chemin du fichier à modifier
ROUTES_NELMIO="config/routes/nelmio_api_doc.yaml"
PACKAGES_NELMIO="config/packages/nelmio_api_doc.yaml"


# Contenu à insérer dans le fichier
NEW_CONTENT_ROUTE="app.swagger_ui:
    path: /api/doc
    methods: GET
    defaults: { _controller: nelmio_api_doc.controller.swagger_ui }"

NEW_CONTENT_PACKAGES="nelmio_api_doc:
    documentation:
        servers:
            - url: $APP_URL
        info:
            title: $APP_NAME
            description: This is an awesome app!
            version: 1.0.0
    areas: # to filter documented areas
        path_patterns:
            - ^/api(?!/doc$) # Accepts routes under /api except /api/doc

#    paths:
#        /api/login_check:
#            post:
#                operationId: postCredentialsItem
#                tags:
#                    - Token
#                summary: Allows to get the token JWT for login.
#                requestBody:
#                    description: Creates a new token JWT
#                    content:
#                        application/json:
#                            schema:
#                                $ref: '#/components/schemas/Credentials'
#                responses:
#                    '200':
#                        description: Récupère le token JWT
#                        content:
#                            application/json:
#                                schema:
#                                    $ref: '#/components/schemas/Token'
#            components:
#                schemas:
#                    Token:
#                        type: object
#                        properties:
#                            token:
#                                type: string
#                                readOnly: true
#                    Credentials:
#                        type: object
#                        properties:
#                            username:
#                                type: string
#                                default: admin@mail.com
#                            password:
#                                type: string
#                                default: password
#                securitySchemes:
#                    bearerAuth:
#                        type: apiKey
#                        in: header
#                        name: Authorization # or another header name
#            security:
#                - bearerAuth: []
#        areas: # to filter documented areas
#            path_patterns:
#                - ^/api(?!/doc$) # Accepts routes under /api except /api/doc"

# Utiliser sed pour remplacer le contenu dans le fichier
echo "$NEW_CONTENT_ROUTE" | tee "$ROUTES_NELMIO" > /dev/null
echo "$NEW_CONTENT_PACKAGES" | tee "$PACKAGES_NELMIO" > /dev/null

### --------- END CONFIG BUNDLES --------- ##
