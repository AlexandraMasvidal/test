<?php
namespace App\Controller;


use App\Service\DockerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class UserController extends AbstractController {


    public function __construct(
        private HttpClientInterface $client,
    ){}


    #[Route('/api/users', name: 'users')]
    public function getUsers() {

        //dd(gethostbyname('host.docker.internal'));
        $test = gethostbyname('host.docker.internal');

        $data = $this->client->request(
            'GET',
            'http://'.$test.':8002/api/user',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ]
        );

        return $this->json([
            'message' => 'Welcome users!',
            "data" => $data->getContent()
        ]);
    }


}
