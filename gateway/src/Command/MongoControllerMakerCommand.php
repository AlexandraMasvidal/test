<?php

namespace App\Command;


use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'make:controller',
    description: 'Creates a new controller.'
)]
class MongoControllerMakerCommand extends Command
{
    public function __construct()
    {
        parent::__construct();

    }

    public function configure() : void
    {
        $this
            ->setHelp('This command allows you to create a new controller')
            ->addArgument('name', InputArgument::OPTIONAL, 'The name of the controller.');
    }

    public function execute(InputInterface $input, OutputInterface $output) : int
    {
        $io = new SymfonyStyle($input, $output);

        $controllerName = empty($input->getArgument('name')) ? '' : $input->getArgument('name');

        if(empty($controllerName)) {
            $controllerName = $io->ask('What is the name of your controller ? (default: Home)', 'Home', function ($answer) use ($controllerName) {
                if (empty($answer)) {
                    throw new \RuntimeException(
                        'The name of the controller cannot be empty.'
                    );
                }

                // exclude specials characters, numbers and spaces
                if  (preg_match('/[\d\s!@#$%^&*()_+{}\[\]:;<>,.?~]/', $answer) > 0) {
                    throw new \RuntimeException('The document name cannot include specials characters or spaces.');
                }

                return $controllerName;
            });
        }

        // remove Controller from the name
        $controllerName = str_replace('Controller', '', $controllerName);
        $controllerName = str_replace('controller', '', $controllerName);
        $controllerName = str_replace(' ', '', $controllerName);

        // capitalize
        $controllerName = ucfirst($controllerName) . 'Controller';

        if(file_exists('src/Controller/' . $controllerName . '.php')) {
            throw new \RuntimeException('The document name cannot include specials characters or spaces.');
        }

        $content = <<<PHP
        <?php
        namespace App\Controller;\n

        use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


        class {$controllerName} extends AbstractController {\n
        
            public function __construct(){
            
            }\n
        
        }
        
        PHP;


        try {
            if(!file_exists('src/Controller')) mkdir('src/Controller');
            $documentPath = getcwd() . '/src/Controller/' . $controllerName . '.php';
            file_put_contents($documentPath, $content);

            $io->success('Class ' . $controllerName . ' has been created successfully !');
        }
        catch (\Throwable $e) {
            // Erreur lors de l'affectation de la propriété
            $io->text($e->getMessage());
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }


}