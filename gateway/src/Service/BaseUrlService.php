<?php

namespace App\Service;

class BaseUrlService
{
    public function __construct()
    {}


    public function getBaseUrl () {
        if ($_ENV['APP_ENV'] === 'dev')  return $this->getBaseUrlDev();
        else return $this->getBaseUrlProd();
    }

    /**
     * @return string of docker host ip address
     */
    public function getDockerInternalHostIpAddress()
    {
        return gethostbyname('host.docker.internal');
    }

    public function getBaseUrlDev () {
        return 'http://'.$this->getDockerInternalHostIpAddress().$_ENV['APP_PORT'];
    }

    public function getBaseUrlProd () {
        return $_ENV['APP_URL'];
    }



}