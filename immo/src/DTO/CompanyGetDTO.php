<?php

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes\Property;

class CompanyGetDTO
{

    public function __construct(

    #[Groups(["read","update", "create"])]
    public readonly string|array|null $id = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $type = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $name = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $websiteUrl = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $address = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $address2 = null,


    #[Groups(["read","create","update"])]
    public readonly string|array|null $zip = null, 

    #[Groups(["read","create","update"])]
    public readonly string|array|null $city = null, 

    #[Groups(["read","create","update"])]
    public readonly string|array|null $country = null, 

    #[Groups(["read","create","update"])]
    public readonly string|array|null $lat = null, 

    #[Groups(["read","create","update"])]
    public readonly string|array|null $lng = null, 

    #[Groups(["read","create","update"])]
    public readonly string|array|null $email = null, 

    #[Groups(["read","create","update"])]
    public readonly string|array|null $telephone = null, 

    #[Groups(["read","create","update"])]
    public readonly string|array|null $pictureUrl = null, 

                  



    #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $createdAt = null, 

                      #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $updatedAt = null, 

                  
    ) {}
}