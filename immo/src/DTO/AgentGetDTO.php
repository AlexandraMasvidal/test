<?php

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes\Property;

class AgentGetDTO
{

    public function __construct(

    #[Groups(["read","update", "create"])]
    public readonly string|array|null $id = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $memberId = null,

                  #[Groups(["read","create","update"])]
    public readonly string|array|null $rsac = null,

                  #[Groups(["read","create","update"])]
    public readonly string|array|null $rsacCity = null,

                  #[Groups(["read","create","update"])]
    public readonly string|array|null $nomination = null,

                  #[Groups(["read","create","update"])]
    public readonly string|array|null $about = null,

              

    ) {}
}