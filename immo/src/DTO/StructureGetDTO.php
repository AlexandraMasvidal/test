<?php

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes\Property;

class StructureGetDTO
{

    public function __construct(

    #[Groups(["read","update", "create"])]
    public readonly string|array|null $id = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $name = null,

                  #[Groups(["read","create","update"])]
    public readonly string|array|null $structureId = null,

              

    ) {}
}