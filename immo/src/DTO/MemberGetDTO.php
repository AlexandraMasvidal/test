<?php

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes\Property;

class MemberGetDTO
{

    public function __construct(

    #[Groups(["read","update", "create"])]
    public readonly string|array|null $id = null,

    #[Groups(["read","create","update"])]
    public readonly bool|array|null $isActive = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $firstname = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $lastname = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $email = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $gender = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $telephone = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $pictureUrl = null,

    #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $createdAt = null,

    #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $updatedAt = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $fromApi = null,

    #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $lastUpdatedInExternalApi = null,

              

    #[Groups(["read","create","update"])]
    public readonly string|array|null $role = null, 

    #[Groups(["read","create","update"])]
    public readonly string|array|null $roleFromExternalApi = null, 

                  
    #[Groups(["read","create","update"])]
    public readonly array|null $externalApiInfos = null,

                  
    ) {}
}