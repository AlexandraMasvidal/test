<?php

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes\Property;

class PropertyImmoGetDTO
{

    public function __construct(

    #[Groups(["read","update", "create"])]
    public readonly string|array|null $id = null,

    #[Groups(["read","create","update"])]
    public readonly bool|array|null $isPublished = null,

                  #[Groups(["read","create","update"])]
    public readonly int|array|null $price = null,

                  #[Groups(["read","create","update"])]
    public readonly string|array|null $feesName = null,

                  #[Groups(["read","create","update"])]
    public readonly float|array|null $vendorFeesAmount = null,

                  #[Groups(["read","create","update"])]
    public readonly float|array|null $buyerFeesAmount = null,

              


    #[Groups(["read","create","update"])]
    public readonly float|array|null $vatAmount = null, 

                      #[Groups(["read","create","update"])]
    public readonly bool|array|null $isSold = null, 

                      #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $soldDate = null, 

                      #[Groups(["read","create","update"])]
    public readonly float|array|null $soldPrice = null, 

                      #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $mandateDate = null, 

                  
    #[Groups(["read","create","update"])]
    public readonly string|array|null $mandateTypeName = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $transactionName = null, 

                      #[Groups(["read","create","update"])]
    public readonly float|array|null $rentIncreasedAmount = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $title = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $publicRef = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $description = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $virtualVisitUrl = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $address = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $address2 = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $zipCode = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $city = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $country = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $lat = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $lng = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $categoryName = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $typeName = null, 

                      #[Groups(["read","create","update"])]
    public readonly string|array|null $subTypeName = null, 

                  
    #[Groups(["read","create","update"])]
    public readonly bool|array|null $prestige = null, 

                      #[Groups(["read","create","update"])]
    public readonly bool|array|null $luxury = null, 

                      #[Groups(["read","create","update"])]
    public readonly bool|array|null $newProgram = null, 

                      #[Groups(["read","create","update"])]
    public readonly int|array|null $area = null, 

                      #[Groups(["read","create","update"])]
    public readonly int|array|null $roomsNumber = null, 

                      #[Groups(["read","create","update"])]
    public readonly int|array|null $bedroomsNumber = null, 

                      #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $createdAt = null, 

                      #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $updatedAt = null, 

                  
    ) {}
}