<?php

namespace App\Service;

use App\Document\Company;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class KwMarketService
{
    public function __construct(
        private DocumentManager $dm,
        private HttpClientInterface $client,
        private string $baseUrl = "https://kwo.kwfrance.com/propapi/",
    )
    {}

    public function getKwMarkets (OutputInterface $output)
    {
        $handle = fopen(getcwd() . "/files-data/apikeysList.csv", "r");
        $markets = $this->dm->getRepository(Company::class)->findAll();
        $row = 1;
        $updatedMarketsList = [];

        while (($line = fgetcsv($handle, 100000, ";")) !== FALSE) {

            if ($row <= 1) {
                $row++;
                continue;
            }

            $apiKey = $line[1];
            $sid = $line[0];

            // check if market already exists
            $market = array_filter($markets, function ($market) use ($sid, $apiKey) {
                    return $market->getSid() === $sid && $market->getApiKey() === $apiKey;
                });

            // get all datas from mykw
            $marketDatas = $this->getMarketInfosFromMyKw($apiKey, $sid);

            // update or create document
            if ($marketDatas) {
                if (empty($market)) {
                    $marketDocument = $this->hydrateMarketDocument($marketDatas, $apiKey, $sid);
                    $output->writeln("");
                    $output->writeln('<info>Market ' . $marketDocument->getName() .' created</info>');
                    $output->writeln("");
                }
                else {
                    $marketDocument = $this->hydrateMarketDocument($marketDatas, $apiKey, $sid, $market[0]);
                    $marketDocument->setUpdatedAt(new \DateTime());
                    $output->writeln("");
                    $output->writeln('<info>Market ' . $marketDocument->getName() .' created</info>');
                    $output->writeln("");
                }

                $this->dm->persist($marketDocument);
                $updatedMarketsList[] = $marketDocument;
            }

            $this->dm->flush();
        }

        return $updatedMarketsList;
    }

    public function getMarketInfosFromMyKw( string $apiKey, string $sid ) {
        //construction of the url with the params
        $urlMcInfo = $this->baseUrl . "informations/mentions?" . "sid=" . $sid . "&key=" . $apiKey;
        $urlMcWebSettings = $this->baseUrl . "informations/websettings?" . "sid=" . $sid . "&key=" . $apiKey;

        //Get the result of the Api call
        $resInfos = $this->client->request('GET', $urlMcInfo)->getContent();
        $resWebsettings = $this->client->request('GET', $urlMcWebSettings)->getContent();

        //converts into an associative array
        $decodedRes1 = json_decode($resInfos, true);
        $decodedRes2 = json_decode($resWebsettings, true);


        //if errors :
        if (
            empty($decodedRes1['status']) || $decodedRes1['status'] !== "OK" ||
            empty($decodedRes2['status']) || $decodedRes2['status'] !== "OK"
        ) return false;

        //if empty results (depending on the status)
        if (is_null($decodedRes1['result']) || is_null($decodedRes2['result'])) return false;

        //if no errors and no empty result we send the properties list
        $mcDatas = array_merge($decodedRes2['result'], $decodedRes1['result']);

        return $mcDatas;
    }

    /**
     * @throws \Exception
     */
    public function hydrateMarketDocument (array $mcDatas, $apiKey, $sid, $marketToUpdate = null) {

        if(is_null($marketToUpdate)) $market = new Company();
        else $market = $marketToUpdate;

        $market
            ->setName($mcDatas['nom_commercial'] ?? "")
            ->setWebsiteUrl($mcDatas['siteweb'] ?? "")
            ->setAddress($mcDatas['adresse'] ?? "")
            ->setAddress2($mcDatas['adresse2'] ?? "")
            ->setZip($mcDatas['codepostal'] ?? "")
            ->setCity($mcDatas['ville'] ?? "")
            ->setCountry($mcDatas['pays'] ?? "")
            ->setLat($mcDatas['geo_lat'] ?? "")
            ->setLng($mcDatas['geo_lng'] ?? "")
            ->setEmail($mcDatas['email'] ?? "")
            ->setTelephone($mcDatas['telephone'] ?? "")
            ->setPictureUrl($mcDatas['image_site'] ?? "");

        $marketLogos = [
            "logoUrl" => $mcDatas['logo_mc'] ?? "",
            "logoOppositeUrl" => $mcDatas['logo_mc_inverse'] ?? "",
            "logoTransparentUrl" => $mcDatas['logo_mc_transparent'] ?? "",
            "logoOppositeTransparentUrl" => $mcDatas['logo_mc_transparent_inverse'] ?? "",
        ];

        $marketSocials = [];
        $mcDatas['url_facebook'] && $marketSocials[] = ["name" => "facebook", "url" => $mcDatas['url_facebook']];
        $mcDatas['url_twitter'] && $marketSocials[] = ["name" => "twitter", "url" => $mcDatas['url_twitter']];
        $mcDatas['url_youtube'] && $marketSocials[] = ["name" => "youtube", "url" => $mcDatas['url_youtube']];
        $mcDatas['url_instagram'] && $marketSocials[] = ["name" => "instagram", "url" => $mcDatas['url_instagram']];
        $mcDatas['url_pinterest'] && $marketSocials[] = ["name" => "pinterest", "url" => $mcDatas['url_pinterest']];

        $legalInfos = [
            "feesUrl" => $mcDatas['bareme_url'] ?? "",
            "businessName" => $mcDatas['raison_sociale'] ?? "",
            "legalStatus" => $mcDatas['code_forme_juridique'] ?? "",
            "siret" => $mcDatas['siret'] ?? "",
            "siren" => $mcDatas['siren'] ?? "",
            "sirenCity" => $mcDatas['siren_ville'] ?? "",
            "capitalStock" => $mcDatas['capital_social'] ?? "",
            "vatNum" => $mcDatas['numtva'] ?? "",
            "proCardNumber" => $mcDatas['cartepro_numero'] ?? "",
            "proCardDate" => $mcDatas['cartepro_date'],
            "proCardValidityDate" => $mcDatas['cartepro_datefin'] ?? "",
            "proCardIssuingAuthority" => $mcDatas['cartepro_delivrepar'] ?? "",
            "guaranteeName" => $mcDatas['garantiefinanciere_assureur'] ?? "",
            "guaranteeAddress" => $mcDatas['garantiefinanciere_adresse'] ?? "",
            "guaranteeNumber" => $mcDatas['garantiefinanciere_numero'] ?? "",
            "guaranteeExpiresAt" => $mcDatas['garantiefinanciere_datefin'] ,
            "guaranteeAmount" => $mcDatas['garantiefinanciere_montant'] ?? "",
            "proIndemnityInsuranceName" => $mcDatas['rcp_nom'] ?? "",
            "proIndemnityInsuranceExpiresAt" => $mcDatas['rcp_datefin'] ,
            "proIndemnityInsuranceNumber" => $mcDatas['rcp_numero'] ?? "",
            "mediatorName" => $mcDatas['mediateur_nom'] ?? "",
            "mediatorAddress" => $mcDatas['mediateur_adresse'] ?? "",
            "mediatorWebsite" => $mcDatas['mediateur_siteweb'] ?? "",
            "mediatorAcquisitionDate" => $mcDatas['mediateur_dateobtention'] ,
        ];

        $apiInfos = [
            "name" => "kw",
            "apiKey" => $apiKey,
            "sid" => $sid,
        ];

        $market
            ->setLogos($marketLogos)
            ->setSocials($marketSocials)
            ->setLegals($legalInfos)
            ->setExternalApiInfos($apiInfos);

        return $market;
    }

}