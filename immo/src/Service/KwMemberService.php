<?php

namespace App\Service;

use App\Document\Agent;
use App\Document\Member;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class KwMemberService
{

    public function __construct(
        private HttpClientInterface $client,
    )
    {
    }

    public function downloadAllMarketMembers( $apiKey, $sid ) : array|false
    {
        $url = "https://kwo.kwfrance.com/propapi/agents/list_members?" . "sid=" . $sid . "&key=" . $apiKey;
        $res = $this->client->request('GET', $url)->getContent();

        //converts into an associative array
        $decodedRes = json_decode($res, true);

        //if errors :
        if (empty($decodedRes['status']) || $decodedRes['status'] !== "OK")
            return false;

        //if empty results (depending on the status)
        $emptyResults = is_null($decodedRes['result']);
        if ($emptyResults)
            return false;


        //if no errors and no empty result we send the members list
        return $decodedRes['result'];
    }

    public function desactiveMembers( array $kwMembers, array $dbMembers ): int
    {
        $kwInactiveMembersIds = array_column(array_filter($kwMembers, fn($member) => $member['id_statut'] === "2"), 'id');

        $membersToDesactive = array_filter($dbMembers, function($member) use ($kwInactiveMembersIds) {
            return in_array($member->getExternalApiInfos()['id'], $kwInactiveMembersIds);
        });

        foreach ($membersToDesactive as $member) {
            if ($member->isActive()) {
                $member->setIsActive(false);
                $this->dm->persist($member);
            }
        }
        return count($membersToDesactive);
    }

    public function getNewMembersFromMyKw ( array $kwMembers, array $dbMembers ) : array
    {
        // get active members
        $kwActiveMembers = array_filter($kwMembers, function ($el) {
            return $el['id_statut'] === "1";
        });

        // get last inserted member
        usort($dbMembers, function ($a, $b) {
            return intval($a->getExternalApiInfos()['id']) - intval($b->getExternalApiInfos()['id']);
        });
        $lastInsertedMember = intval($dbMembers[0]->getExternalApiInfos()['id']);

        // get all new members of my kw
        $newMembers = array_filter($kwActiveMembers, function ($el) use ($lastInsertedMember) {
            return intval($el['id']) > $lastInsertedMember;
        });

        return $newMembers;
    }

    public function createMembersAndAgents( $kwMemberData, $mcId ) : void
    {

        foreach($kwMemberData as $kwMember) {
            $newMember = new Member();
            $newMember
                ->setIsActive(true)
                ->setFirstname($kwMember['prenom'])
                ->setLastname($kwMember['nom'])
                ->setEmail($kwMember['email'])
                ->setTelephone($kwMember['telephone'])
                ->setGender($kwMember['genre_name'])
                ->setPictureUrl($kwMember['picture'] ?? null)
                ->setConnexions([]);

            $kwRole = $kwMember['role_name'];
            $newMember->setRole($kwRole === "Agent" ? "agent" : "staff");

            $externalApiInfos = [
                'name'       => "kw",
                'id'         => $kwMember['id'],
                'lastUpdate' => $kwMember['date_modified'],
                'mcId'       => $kwMember['id_mc'],
                'role'       => $kwRole,
            ];

            $connexions = [
                [
                    'type' => "market",
                    'id' => $mcId,
                ]
            ];

            $newMember->setExternalApiInfos($externalApiInfos);
            $newMember->setConnexions($connexions);

            $this->dm->persist($newMember);

            $this->createAgentFromMyKwData($kwMember, $newMember->getId());
        }

        $this->dm->flush();
    }

    public function createAgentFromMyKwData( array $agentsDatas, string $memberId ) : void
    {
        foreach($agentsDatas as $agentData) {
            $newAgent = new Agent();
            $newAgent
                ->setMemberId($memberId)
                ->setRsac($agentData['rsac'] ?? null)
                ->setRsacCity($agentData['rsac_ville'] ?? null)
                ->setSector([])
                ->setNomination("")
                ->setAbout("");

            $this->dm->persist($newAgent);
        }
    }

    public function updateMembers ($kwMembers, $dbMembers) {

    }

    public function updateAgents() {

    }

}