<?php

namespace App\Enum;

enum ConnectedToEnums : string
{
    case AGENT = "Agent";

    case MARKET = "Market";

    case MEGATEAM  = "MegaTeam";

    case TEAM = "Team";

    public static function getAll() : array{
        return [
            self::MARKET->value,
            self::AGENT->value,
            self::MEGATEAM->value,
            self::TEAM->value
        ];
    }

    public static function getPropertyConnexions() : array{
        return [
            self::MARKET->value,
            self::AGENT->value,
            self::MEGATEAM->value,
            self::TEAM->value
        ];
    }

    public static function getMemberConnexions() : array{
        return [
            self::MARKET->value,
            self::MEGATEAM->value,
            self::TEAM->value
        ];
    }
}
