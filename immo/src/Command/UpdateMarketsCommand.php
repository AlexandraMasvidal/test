<?php

namespace App\Command;

use App\Document\Company;
use App\Service\KwMarketService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'update:markets')]
class UpdateMarketsCommand extends Command
{
    public function __construct(
        private DocumentManager $dm, private KwMarketService $kwMarketService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        // todo : add args for delete and update one market
    }
    protected function execute(InputInterface $input, OutputInterface $output) {

        $output->writeln("");
        $output->writeln('<info>start update Markets centers</info>');
        $output->writeln("");
        $marketCenters = $this->dm->getRepository(Company::class)->findAll();
        $total = count($marketCenters);

        if(empty($marketCenters)) {
            $output->writeln("");
            $output->writeln('<info>Creating markets...</info>');
            $output->writeln("");

            $this->kwMarketService->getKwMarkets($output);

            return Command::SUCCESS;
        }

        foreach($marketCenters as $key => $mc) {

            $apiId = $mc->getExternalApiInfos()['apiKey'];
            $sid = $mc->getExternalApiInfos()['sid'];

            $output->writeln("");
            $output->writeln('<info>UPDATE </info>' . $mc->getName() . '... (' . ($key + 1) . '/' . $total . ')');
            $output->writeln("");

            // if missing data -> error
            if(empty($apiId) || empty($sid)) {
                $output->writeln("");
                $output->writeln('<error>Missing apiKey or sid</error>');
                $output->writeln("");
                return Command::FAILURE;
            }

            // get datas from api
            $mcDatas = $this->kwMarketService->getMarketInfosFromMyKw($apiId, $sid);

            // if empty data -> error
            if(!$mcDatas) {
                $output->writeln("");
                $output->writeln('<error>Empty datas or an error occured during the call to myKw api</error>');
                $output->writeln("");
                return Command::FAILURE;
            }

            // update market with datas
            $marketUpdated = $this->kwMarketService->hydrateMarketDocument($mcDatas, $apiId, $sid, $mc );

            $this->dm->persist($marketUpdated);
        }

        $this->dm->flush();

        return Command::SUCCESS;

    }

}