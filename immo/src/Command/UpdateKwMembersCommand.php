<?php

namespace App\Command;

use App\Document\Agent;
use App\Document\Company;
use App\Document\Member;
use App\Service\KwMemberService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'update:members:kw')]
class UpdateKwMembersCommand extends Command
{

    public function __construct(
        private DocumentManager $dm,
        private KwMemberService $kwMemberService
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $marketCenters = $this->dm->getRepository(Company::class)->findBy(['externalApiInfos.name' => "kw"]);

        foreach ($marketCenters as $mc) {
            $allMcMembersFromMyKw = $this->kwMemberService->downloadAllMarketMembers($mc->getExternalApiInfos()['apiKey'], $mc->getExternalApiInfos()['sid']);
            $dbMembers = $this->dm->getRepository(Member::class)->findBy(['externalApiInfos.name' => "kw", 'externalApiInfos.mcId' => $mc->getId()]);

            // desactive members
            $this->kwMemberService->desactiveMembers($allMcMembersFromMyKw, $dbMembers);

            // todo : request to disable websites linked and revoque users access

            // create new members and agents
            $newKwMembers = $this->kwMemberService->getNewMembersFromMyKw($allMcMembersFromMyKw, $dbMembers);
            $this->kwMemberService->createMembersAndAgents($newKwMembers, $mc->getId());

        }

        return Command::SUCCESS;
    }


}