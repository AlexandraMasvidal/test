<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use OpenApi\Attributes as OA;
use Ama\SymfonyMongoMakerBundle\DTO\QueryDTO;
use Ama\SymfonyMongoMakerBundle\Service\QueryBuilderFromUrlService;
use App\Document\Structure;
use App\DTO\StructureGetDTO;
use Nelmio\ApiDocBundle\Annotation\Model;


class StructureController extends AbstractController
{

    public function __construct(
        private DocumentManager $dm,
        private ValidatorInterface $validator,
        private SerializerInterface $serializer,
        private QueryBuilderFromUrlService $queryBuilderFromUrlService
    ){}


    #[Route("/api/structure", methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Returns an array of structure',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: Structure::class, groups: ['read'])),
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string|bool',
                ),
            ],
            type: 'array',
            items: new OA\Items(ref: new Model(type: Structure::class, groups: ['read']))
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'id- field of StructureGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'name',
        description: 'name - field of StructureGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'structureId',
        description: 'structureId - field of StructureGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'sort',
        description: 'array to sort query by fields name with asc or desc values. Example sort[date]=asc&sort[name]=desc will sort the results by date and by name',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'nested',
        description: 'array to search into nested objects fields. Example nested[user.website.id]=4 will search for a user with a website with id 4',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    public function findBy ( #[MapQueryString] ?StructureGetDTO $structuregetdto, #[MapQueryString] ?QueryDTO $queryDTO ) :JsonResponse
    {
        // if no params, return the result of findAll
        $result = $this->queryBuilderFromUrlService->getResult($this->dm, Structure::class, $structuregetdto, $queryDTO);

        // normalize results
        $normalizedResult = $this->serializer->normalize($result, null, ["groups" => ['read']]);

        // return result
        return new JsonResponse([
            "data" => $normalizedResult,
            "error" => false
        ], Response::HTTP_OK);
    }





    #[Route("/api/structure/{id}", methods: ['PUT'])]
    #[OA\Response(
        response: 200,
        description: 'Returns the structure updated',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    ref: new Model(type: Structure::class, groups: ['read']),
                    type: 'object'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'structure id',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    public function update ( #[MapRequestPayload] StructureGetDTO $structuregetdto, $id ) :JsonResponse
    {
        $documentToUpdate = $this->dm->find(Structure::class, $id);

        if(empty($documentToUpdate)) {
            return new JsonResponse([
                "data" => false,
                "error" => "no test matching with id " . $id . " in database"
            ], Response::HTTP_OK);
        }


        $structuregetdto = array_filter(json_decode(json_encode($structuregetdto), true), fn($value) => $value !== null);
        $this->dm->getHydratorFactory()->hydrate($documentToUpdate, $structuregetdto);
        $this->dm->persist($documentToUpdate);
        $this->dm->flush();



        // return result

        return new JsonResponse([
            "data" => $this->serializer->normalize($documentToUpdate, null, ['groups' => ['read']] ),
            "error" => false
        ], Response::HTTP_OK);

    }


    #[Route("/api/structure", methods: ['POST'])]
    #[OA\Response(
        response: 200,
        description: "Returns an object with 'data' key (object | false) that provides the new document created and 'error' key (string | false)",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    ref: new Model(type: Structure::class, groups: ['read']),
                    type: 'object'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    public function create( #[MapRequestPayload] structure $structure ): JsonResponse
    {
        $errors = $this->validator->validate($structure, null, ['create']);

        if(count($errors) > 0) {
            $errorsString = implode(', ', array_map(fn($error) => $error->getMessage(), iterator_to_array($errors)));
            return new JsonResponse([
                "data" => false,
                "error" => $errorsString
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->dm->persist($structure);
        $this->dm->flush();

        $structure = $this->serializer->normalize($structure, null, ['groups' => ['read']] );

        return new JsonResponse([
            "data" => $structure,
            "error" => false
        ], Response::HTTP_OK);

    }

    #[Route("/api/structure/{id}", methods: ['DELETE'])]
    #[OA\Response(
        response: 200,
        description: "Returns an object with 'data' key (string | false) that confirm the deletion and 'error' key (string | false)",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'string'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'structure id',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    public function delete ( $id ) :JsonResponse
    {
        $document = $this->dm->find(Structure::class, $id);

        if(empty($document)) {
            return new JsonResponse([
                "data" => false,
                "error" => "no test matching with this id in database"
            ], Response::HTTP_BAD_REQUEST);
        }
        else {
            $this->dm->remove($document);
            $this->dm->flush();
        }
        return new JsonResponse([
            "data" => "document removed successfully",
            "error" => false
        ], Response::HTTP_OK);
    }
}