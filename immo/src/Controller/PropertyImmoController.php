<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use OpenApi\Attributes as OA;
use Ama\SymfonyMongoMakerBundle\DTO\QueryDTO;
use Ama\SymfonyMongoMakerBundle\Service\QueryBuilderFromUrlService;
use App\Document\PropertyImmo;
use App\DTO\PropertyImmoGetDTO;
use Nelmio\ApiDocBundle\Annotation\Model;


class PropertyImmoController extends AbstractController
{

    public function __construct(
        private DocumentManager $dm,
        private ValidatorInterface $validator,
        private SerializerInterface $serializer,
        private QueryBuilderFromUrlService $queryBuilderFromUrlService
    ){}


    #[Route("/api/propertyimmo", methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Returns an array of propertyimmo',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: PropertyImmo::class, groups: ['read'])),
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string|bool',
                ),
            ],
            type: 'array',
            items: new OA\Items(ref: new Model(type: PropertyImmo::class, groups: ['read']))
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'id- field of PropertyImmoGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'isPublished',
        description: 'isPublished - field of PropertyImmoGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'bool|bool[]|null')
    )]
    #[OA\Parameter(
        name: 'price',
        description: 'price - field of PropertyImmoGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'int|int[]|null')
    )]
    #[OA\Parameter(
        name: 'feesName',
        description: 'feesName - field of PropertyImmoGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'vendorFeesAmount',
        description: 'vendorFeesAmount - field of PropertyImmoGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'float|float[]|null')
    )]
    #[OA\Parameter(
        name: 'buyerFeesAmount',
        description: 'buyerFeesAmount - field of PropertyImmoGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'float|float[]|null')
    )]
    #[OA\Parameter(
        name: 'sort',
        description: 'array to sort query by fields name with asc or desc values. Example sort[date]=asc&sort[name]=desc will sort the results by date and by name',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'nested',
        description: 'array to search into nested objects fields. Example nested[user.website.id]=4 will search for a user with a website with id 4',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'vatAmount',
        description: 'vatAmount - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'float|float[]|null')
    )]
    #[OA\Parameter(
        name: 'isSold',
        description: 'isSold - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'bool|bool[]|null')
    )]
    #[OA\Parameter(
        name: 'soldDate',
        description: 'soldDate - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: '\DateTime|\DateTime[]|null')
    )]
    #[OA\Parameter(
        name: 'soldPrice',
        description: 'soldPrice - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'float|float[]|null')
    )]
    #[OA\Parameter(
        name: 'mandateDate',
        description: 'mandateDate - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: '\DateTime|\DateTime[]|null')
    )]
    #[OA\Parameter(
        name: 'mandateTypeName',
        description: 'mandateTypeName - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'transactionName',
        description: 'transactionName - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'rentIncreasedAmount',
        description: 'rentIncreasedAmount - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'float|float[]|null')
    )]
    #[OA\Parameter(
        name: 'title',
        description: 'title - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'publicRef',
        description: 'publicRef - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'description',
        description: 'description - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'virtualVisitUrl',
        description: 'virtualVisitUrl - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'address',
        description: 'address - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'address2',
        description: 'address2 - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'zipCode',
        description: 'zipCode - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'city',
        description: 'city - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'country',
        description: 'country - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'lat',
        description: 'lat - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'lng',
        description: 'lng - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'categoryName',
        description: 'categoryName - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'typeName',
        description: 'typeName - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'subTypeName',
        description: 'subTypeName - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'prestige',
        description: 'prestige - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'bool|bool[]|null')
    )]
    #[OA\Parameter(
        name: 'luxury',
        description: 'luxury - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'bool|bool[]|null')
    )]
    #[OA\Parameter(
        name: 'newProgram',
        description: 'newProgram - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'bool|bool[]|null')
    )]
    #[OA\Parameter(
        name: 'area',
        description: 'area - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'int|int[]|null')
    )]
    #[OA\Parameter(
        name: 'roomsNumber',
        description: 'roomsNumber - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'int|int[]|null')
    )]
    #[OA\Parameter(
        name: 'bedroomsNumber',
        description: 'bedroomsNumber - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: 'int|int[]|null')
    )]
    #[OA\Parameter(
        name: 'createdAt',
        description: 'createdAt - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: '\DateTime|\DateTime[]|null')
    )]
    #[OA\Parameter(
        name: 'updatedAt',
        description: 'updatedAt - field of PropertyImmoDTO',
        in: 'query',
        schema: new OA\Schema(type: '\DateTime|\DateTime[]|null')
    )]
    public function findBy ( #[MapQueryString] ?PropertyImmoGetDTO $propertyimmogetdto, #[MapQueryString] ?QueryDTO $queryDTO ) :JsonResponse
    {
        // if no params, return the result of findAll
        $result = $this->queryBuilderFromUrlService->getResult($this->dm, PropertyImmo::class, $propertyimmogetdto, $queryDTO);

        // normalize results
        $normalizedResult = $this->serializer->normalize($result, null, ["groups" => ['read']]);

        // return result
        return new JsonResponse([
            "data" => $normalizedResult,
            "error" => false
        ], Response::HTTP_OK);
    }





    #[Route("/api/propertyimmo/{id}", methods: ['PUT'])]
    #[OA\Response(
        response: 200,
        description: 'Returns the propertyimmo updated',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    ref: new Model(type: PropertyImmo::class, groups: ['read']),
                    type: 'object'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'propertyimmo id',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    public function update ( #[MapRequestPayload] PropertyImmoGetDTO $propertyimmogetdto, $id ) :JsonResponse
    {
        $documentToUpdate = $this->dm->find(PropertyImmo::class, $id);

        if(empty($documentToUpdate)) {
            return new JsonResponse([
                "data" => false,
                "error" => "no test matching with id " . $id . " in database"
            ], Response::HTTP_OK);
        }


        $propertyimmogetdto = array_filter(json_decode(json_encode($propertyimmogetdto), true), fn($value) => $value !== null);
        $this->dm->getHydratorFactory()->hydrate($documentToUpdate, $propertyimmogetdto);
        $this->dm->persist($documentToUpdate);
        $this->dm->flush();



        // return result

        return new JsonResponse([
            "data" => $this->serializer->normalize($documentToUpdate, null, ['groups' => ['read']] ),
            "error" => false
        ], Response::HTTP_OK);

    }


    #[Route("/api/propertyimmo", methods: ['POST'])]
    #[OA\Response(
        response: 200,
        description: "Returns an object with 'data' key (object | false) that provides the new document created and 'error' key (string | false)",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    ref: new Model(type: PropertyImmo::class, groups: ['read']),
                    type: 'object'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    public function create( #[MapRequestPayload] propertyimmo $propertyimmo ): JsonResponse
    {
        $errors = $this->validator->validate($propertyimmo, null, ['create']);

        if(count($errors) > 0) {
            $errorsString = implode(', ', array_map(fn($error) => $error->getMessage(), iterator_to_array($errors)));
            return new JsonResponse([
                "data" => false,
                "error" => $errorsString
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->dm->persist($propertyimmo);
        $this->dm->flush();

        $propertyimmo = $this->serializer->normalize($propertyimmo, null, ['groups' => ['read']] );

        return new JsonResponse([
            "data" => $propertyimmo,
            "error" => false
        ], Response::HTTP_OK);

    }

    #[Route("/api/propertyimmo/{id}", methods: ['DELETE'])]
    #[OA\Response(
        response: 200,
        description: "Returns an object with 'data' key (string | false) that confirm the deletion and 'error' key (string | false)",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'string'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'propertyimmo id',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    public function delete ( $id ) :JsonResponse
    {
        $document = $this->dm->find(PropertyImmo::class, $id);

        if(empty($document)) {
            return new JsonResponse([
                "data" => false,
                "error" => "no test matching with this id in database"
            ], Response::HTTP_BAD_REQUEST);
        }
        else {
            $this->dm->remove($document);
            $this->dm->flush();
        }
        return new JsonResponse([
            "data" => "document removed successfully",
            "error" => false
        ], Response::HTTP_OK);
    }
}