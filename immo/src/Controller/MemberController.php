<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use OpenApi\Attributes as OA;
use Ama\SymfonyMongoMakerBundle\DTO\QueryDTO;
use Ama\SymfonyMongoMakerBundle\Service\QueryBuilderFromUrlService;
use App\Document\Member;
use App\DTO\MemberGetDTO;
use Nelmio\ApiDocBundle\Annotation\Model;


class MemberController extends AbstractController
{

    public function __construct(
        private DocumentManager $dm,
        private ValidatorInterface $validator,
        private SerializerInterface $serializer,
        private QueryBuilderFromUrlService $queryBuilderFromUrlService
    ){}


    #[Route("/api/member", methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Returns an array of member',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: Member::class, groups: ['read'])),
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string|bool',
                ),
            ],
            type: 'array',
            items: new OA\Items(ref: new Model(type: Member::class, groups: ['read']))
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'id- field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'isActive',
        description: 'isActive - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'bool|bool[]|null')
    )]
    #[OA\Parameter(
        name: 'firstname',
        description: 'firstname - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'lastname',
        description: 'lastname - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'email',
        description: 'email - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'gender',
        description: 'gender - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'telephone',
        description: 'telephone - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'pictureUrl',
        description: 'pictureUrl - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'createdAt',
        description: 'createdAt - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: '\DateTime|\DateTime[]|null')
    )]
    #[OA\Parameter(
        name: 'updatedAt',
        description: 'updatedAt - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: '\DateTime|\DateTime[]|null')
    )]
    #[OA\Parameter(
        name: 'fromApi',
        description: 'fromApi - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'lastUpdatedInExternalApi',
        description: 'lastUpdatedInExternalApi - field of MemberGetDTO',
        in: 'query',
        schema: new OA\Schema(type: '\DateTime|\DateTime[]|null')
    )]
    #[OA\Parameter(
        name: 'sort',
        description: 'array to sort query by fields name with asc or desc values. Example sort[date]=asc&sort[name]=desc will sort the results by date and by name',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'nested',
        description: 'array to search into nested objects fields. Example nested[user.website.id]=4 will search for a user with a website with id 4',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'role',
        description: 'role - field of MemberDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]    #[OA\Parameter(
        name: 'roleFromExternalApi',
        description: 'roleFromExternalApi - field of MemberDTO',
        in: 'query',
        schema: new OA\Schema(type: 'string|string[]|null')
    )]
    #[OA\Parameter(
        name: 'externalApiInfos',
        description: 'externalApiInfos - field of MemberDTO',
        in: 'query',
        schema: new OA\Schema(type: 'array|array[]|null')
    )]
    public function findBy ( #[MapQueryString] ?MemberGetDTO $membergetdto, #[MapQueryString] ?QueryDTO $queryDTO ) :JsonResponse
    {
        // if no params, return the result of findAll
        $result = $this->queryBuilderFromUrlService->getResult($this->dm, Member::class, $membergetdto, $queryDTO);

        // normalize results
        $normalizedResult = $this->serializer->normalize($result, null, ["groups" => ['read']]);

        // return result
        return new JsonResponse([
            "data" => $normalizedResult,
            "error" => false
        ], Response::HTTP_OK);
    }





    #[Route("/api/member/{id}", methods: ['PUT'])]
    #[OA\Response(
        response: 200,
        description: 'Returns the member updated',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    ref: new Model(type: Member::class, groups: ['read']),
                    type: 'object'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'member id',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    public function update ( #[MapRequestPayload] MemberGetDTO $membergetdto, $id ) :JsonResponse
    {
        $documentToUpdate = $this->dm->find(Member::class, $id);

        if(empty($documentToUpdate)) {
            return new JsonResponse([
                "data" => false,
                "error" => "no test matching with id " . $id . " in database"
            ], Response::HTTP_OK);
        }


        $membergetdto = array_filter(json_decode(json_encode($membergetdto), true), fn($value) => $value !== null);
        $this->dm->getHydratorFactory()->hydrate($documentToUpdate, $membergetdto);
        $this->dm->persist($documentToUpdate);
        $this->dm->flush();



        // return result

        return new JsonResponse([
            "data" => $this->serializer->normalize($documentToUpdate, null, ['groups' => ['read']] ),
            "error" => false
        ], Response::HTTP_OK);

    }


    #[Route("/api/member", methods: ['POST'])]
    #[OA\Response(
        response: 200,
        description: "Returns an object with 'data' key (object | false) that provides the new document created and 'error' key (string | false)",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    ref: new Model(type: Member::class, groups: ['read']),
                    type: 'object'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    public function create( #[MapRequestPayload] member $member ): JsonResponse
    {
        $errors = $this->validator->validate($member, null, ['create']);

        if(count($errors) > 0) {
            $errorsString = implode(', ', array_map(fn($error) => $error->getMessage(), iterator_to_array($errors)));
            return new JsonResponse([
                "data" => false,
                "error" => $errorsString
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->dm->persist($member);
        $this->dm->flush();

        $member = $this->serializer->normalize($member, null, ['groups' => ['read']] );

        return new JsonResponse([
            "data" => $member,
            "error" => false
        ], Response::HTTP_OK);

    }

    #[Route("/api/member/{id}", methods: ['DELETE'])]
    #[OA\Response(
        response: 200,
        description: "Returns an object with 'data' key (string | false) that confirm the deletion and 'error' key (string | false)",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'data',
                    type: 'string'
                ),
                new OA\Property(
                    property: 'error',
                    type: 'string | false'
                )
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'member id',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    public function delete ( $id ) :JsonResponse
    {
        $document = $this->dm->find(Member::class, $id);

        if(empty($document)) {
            return new JsonResponse([
                "data" => false,
                "error" => "no test matching with this id in database"
            ], Response::HTTP_BAD_REQUEST);
        }
        else {
            $this->dm->remove($document);
            $this->dm->flush();
        }
        return new JsonResponse([
            "data" => "document removed successfully",
            "error" => false
        ], Response::HTTP_OK);
    }
}