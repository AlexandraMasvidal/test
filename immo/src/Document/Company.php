<?php
namespace App\Document;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use OpenApi\Attributes\Property;

#[Document]

class Company {


    #[Id]
    #[Groups(["read","update", "create"])]
    private string $id;

    #[Assert\NotBlank(message: "type is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $type; 

    #[Assert\NotBlank(message: "name is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $name; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $websiteUrl; 

    #[Assert\NotBlank(message: "address is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $address; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $address2; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $zip; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $city; 

    #[Assert\NotBlank(message: "country is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $country; 

    #[Assert\NotBlank(message: "lat is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $lat; 

    #[Assert\NotBlank(message: "lng is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $lng; 

    #[Assert\NotBlank(message: "email is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $email; 

    #[Assert\NotBlank(message: "telephone is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $telephone; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $pictureUrl; 

    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "hash" , nullable: true)]
    private ?array $logos;


    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "collection" , nullable: true)]
    private ?array $socials; 

    #[Assert\NotBlank(message: "legals is required", groups: ["create"])]
    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "hash" )]
    private array $legals; 


    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "hash" , nullable: true)]
    private ?array $externalApiInfos; 


    #[Assert\NotBlank(message: "createdAt is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "date" )]
    private \DateTime $createdAt; 

    #[Assert\NotBlank(message: "updatedAt is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "date" )]
    private \DateTime $updatedAt; 


    public function __construct(){
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Company
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Company
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWebsiteUrl(): ?string
    {
        return $this->websiteUrl;
    }

    /**
     * @param string|null $websiteUrl
     * @return Company
     */
    public function setWebsiteUrl(?string $websiteUrl): self
    {
        $this->websiteUrl = $websiteUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Company
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    /**
     * @param string|null $address2
     * @return Company
     */
    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @param string|null $zip
     * @return Company
     */
    public function setZip(?string $zip): self
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return Company
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return Company
     */
    public function setCountry(string $country): self
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getLat(): string
    {
        return $this->lat;
    }

    /**
     * @param string $lat
     * @return Company
     */
    public function setLat(string $lat): self
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return string
     */
    public function getLng(): string
    {
        return $this->lng;
    }

    /**
     * @param string $lng
     * @return Company
     */
    public function setLng(string $lng): self
    {
        $this->lng = $lng;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Company
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return Company
     */
    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPictureUrl(): ?string
    {
        return $this->pictureUrl;
    }

    /**
     * @param string|null $pictureUrl
     * @return Company
     */
    public function setPictureUrl(?string $pictureUrl): self
    {
        $this->pictureUrl = $pictureUrl;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getLogos(): ?array
    {
        return $this->logos;
    }

    /**
     * @param array|null $logos
     * @return Company
     */
    public function setLogos(?array $logos): self
    {
        $this->logos = $logos;
        return $this;
    }


    /**
     * @return array|null
     */
    public function getSocials(): ?array
    {
        return $this->socials;
    }

    /**
     * @param array|null $socials
     * @return Company
     */
    public function setSocials(?array $socials): self
    {
        $this->socials = $socials;
        return $this;
    }

    /**
     * @return array
     */
    public function getLegals(): array
    {
        return $this->legals;
    }

    /**
     * @param array $legals
     * @return Company
     */
    public function setLegals(array $legals): self
    {
        $this->legals = $legals;
        return $this;
    }


    /**
     * @return array|null
     */
    public function getExternalApiInfos(): ?array
    {
        return $this->externalApiInfos;
    }

    /**
     * @param array|null $externalApiInfos
     * @return Company
     */
    public function setExternalApiInfos(?array $externalApiInfos): self
    {
        $this->externalApiInfos = $externalApiInfos;
        return $this;
    }



    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Company
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Company
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }


}