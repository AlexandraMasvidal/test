<?php
namespace App\Document;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use OpenApi\Attributes\Property;

#[Document]

class Agent {


    #[Id]
    #[Groups(["read","update", "create"])]
    private string $id;

    #[Assert\NotBlank(message: "memberId is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $memberId; 

    #[Assert\NotBlank(message: "rsac is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $rsac; 

    #[Assert\NotBlank(message: "rsacCity is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $rsacCity; 

    #[Assert\NotBlank(message: "sector is required", groups: ["create"])]
    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "collection" )]
    private array $sector; 

    #[Assert\NotBlank(message: "nomination is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $nomination; 

    #[Assert\NotBlank(message: "about is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $about; 

    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMemberId(): string
    {
        return $this->memberId;
    }

    /**
     * @param string $memberId
     * @return Agent
     */
    public function setMemberId(string $memberId): self
    {
        $this->memberId = $memberId;
        return $this;
    }

    /**
     * @return string
     */
    public function getRsac(): string
    {
        return $this->rsac;
    }

    /**
     * @param string $rsac
     * @return Agent
     */
    public function setRsac(string $rsac): self
    {
        $this->rsac = $rsac;
        return $this;
    }

    /**
     * @return string
     */
    public function getRsacCity(): string
    {
        return $this->rsacCity;
    }

    /**
     * @param string $rsacCity
     * @return Agent
     */
    public function setRsacCity(string $rsacCity): self
    {
        $this->rsacCity = $rsacCity;
        return $this;
    }

    /**
     * @return array
     */
    public function getSector(): array
    {
        return $this->sector;
    }

    /**
     * @param array $sector
     * @return Agent
     */
    public function setSector(array $sector): self
    {
        $this->sector = $sector;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomination(): string
    {
        return $this->nomination;
    }

    /**
     * @param string $nomination
     * @return Agent
     */
    public function setNomination(string $nomination): self
    {
        $this->nomination = $nomination;
        return $this;
    }

    /**
     * @return string
     */
    public function getAbout(): string
    {
        return $this->about;
    }

    /**
     * @param string $about
     * @return Agent
     */
    public function setAbout(string $about): self
    {
        $this->about = $about;
        return $this;
    }


}