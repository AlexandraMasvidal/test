<?php
namespace App\Document;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use OpenApi\Attributes\Property;

#[Document]

class Structure {


    #[Id]
    #[Groups(["read","update", "create"])]
    private string $id;

    #[Assert\NotBlank(message: "name is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $name; 

    #[Assert\NotBlank(message: "structureId is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $structureId; 

    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Structure
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getStructureId(): string
    {
        return $this->structureId;
    }

    /**
     * @param string $structureId
     * @return Structure
     */
    public function setStructureId(string $structureId): self
    {
        $this->structureId = $structureId;
        return $this;
    }


}