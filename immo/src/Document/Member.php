<?php
namespace App\Document;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use OpenApi\Attributes\Property;

#[Document]

class Member {


    #[Id]
    #[Groups(["read","update", "create"])]
    private string $id;

    #[Assert\NotBlank(message: "isActive is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "bool" )]
    private bool $isActive = true;

    #[Assert\NotBlank(message: "firstname is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $firstname = "";

    #[Assert\NotBlank(message: "lastname is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $lastname = "";

    #[Assert\NotBlank(message: "email is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $email = "";

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $gender = "";

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $telephone = "";

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $pictureUrl = "";

    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "collection" , nullable: true)]
    private ?array $connexions = [];

    #[Assert\NotBlank(message: "createdAt is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "date" )]
    private \DateTime $createdAt; 

    #[Assert\NotBlank(message: "updatedAt is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "date" )]
    private \DateTime $updatedAt; 


    #[Assert\NotBlank(message: "role is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $role; // agent / staff

    #[Groups(["read","update", "create"])]
    #[Field(type: "hash" , nullable: true)]
    private ?array $externalApiInfos; 


    public function __construct(){
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->connexions = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return Member
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return Member
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return Member
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Member
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     * @return Member
     */
    public function setGender(?string $gender): self
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    /**
     * @param string|null $telephone
     * @return Member
     */
    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPictureUrl(): ?string
    {
        return $this->pictureUrl;
    }

    /**
     * @param string|null $pictureUrl
     * @return Member
     */
    public function setPictureUrl(?string $pictureUrl): self
    {
        $this->pictureUrl = $pictureUrl;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getConnexions(): ?array
    {
        return $this->connexions;
    }

    /**
     * @param array|null $connexions
     * @return Member
     */
    public function setConnexions(?array $connexions): self
    {
        $this->connexions = $connexions;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Member
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Member
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }



    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return Member
     */
    public function setRole(string $role): self
    {
        $this->role = $role;
        return $this;
    }


    /**
     * @return array|null
     */
    public function getExternalApiInfos(): ?array
    {
        return $this->externalApiInfos;
    }

    /**
     * @param array|null $externalApiInfos
     * @return Member
     */
    public function setExternalApiInfos(?array $externalApiInfos): self
    {
        $this->externalApiInfos = $externalApiInfos;
        return $this;
    }


}