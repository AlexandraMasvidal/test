<?php
namespace App\Document;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use OpenApi\Attributes\Property;

#[Document]

class PropertyImmo {


    #[Id]
    #[Groups(["read","update", "create"])]
    private string $id;

    #[Assert\NotBlank(message: "isPublished is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "bool" )]
    private bool $isPublished; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "int" , nullable: true)]
    private ?int $price; 

    #[Assert\NotBlank(message: "feesName is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $feesName; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "float" , nullable: true)]
    private ?float $vendorFeesAmount; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "float" , nullable: true)]
    private ?float $buyerFeesAmount; 


    #[Assert\NotBlank(message: "vatAmount is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "float" )]
    private float $vatAmount; 

    #[Assert\NotBlank(message: "isSold is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "bool" )]
    private bool $isSold; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "date" , nullable: true)]
    private ?\DateTime $soldDate; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "float" , nullable: true)]
    private ?float $soldPrice; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "date" , nullable: true)]
    private ?\DateTime $mandateDate; 


    #[Assert\NotBlank(message: "mandateTypeName is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $mandateTypeName; 

    #[Assert\NotBlank(message: "transactionName is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $transactionName; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "float" , nullable: true)]
    private ?float $rentIncreasedAmount; 

    #[Assert\NotBlank(message: "title is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "string" )]
    private string $title; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $publicRef; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $description; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $virtualVisitUrl; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $address; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $address2; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $zipCode; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $city; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $country; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $lat; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $lng; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $categoryName; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $typeName; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $subTypeName; 

    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "collection" , nullable: true)]
    private ?array $features; 

    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "collection" , nullable: true)]
    private ?array $pictures; 

    #[Property(type: "string[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "collection" , nullable: true)]
    private ?array $tags; 


    #[Groups(["read","update", "create"])]
    #[Field(type: "bool" , nullable: true)]
    private ?bool $prestige; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "bool" , nullable: true)]
    private ?bool $luxury; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "bool" , nullable: true)]
    private ?bool $newProgram; 

    #[Assert\NotBlank(message: "area is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "int" )]
    private int $area; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "int" , nullable: true)]
    private ?int $roomsNumber; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "int" , nullable: true)]
    private ?int $bedroomsNumber; 

    #[Assert\NotBlank(message: "reportTo is required", groups: ["create"])]
    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "collection" )]
    private array $reportTo; 

    #[Assert\NotBlank(message: "createdAt is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "date" )]
    private \DateTime $createdAt; 

    #[Assert\NotBlank(message: "updatedAt is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "date" )]
    private \DateTime $updatedAt; 


    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getIsPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool $isPublished
     * @return PropertyImmo
     */
    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int|null $price
     * @return PropertyImmo
     */
    public function setPrice(?int $price): self
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getFeesName(): string
    {
        return $this->feesName;
    }

    /**
     * @param string $feesName
     * @return PropertyImmo
     */
    public function setFeesName(string $feesName): self
    {
        $this->feesName = $feesName;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getVendorFeesAmount(): ?float
    {
        return $this->vendorFeesAmount;
    }

    /**
     * @param float|null $vendorFeesAmount
     * @return PropertyImmo
     */
    public function setVendorFeesAmount(?float $vendorFeesAmount): self
    {
        $this->vendorFeesAmount = $vendorFeesAmount;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getBuyerFeesAmount(): ?float
    {
        return $this->buyerFeesAmount;
    }

    /**
     * @param float|null $buyerFeesAmount
     * @return PropertyImmo
     */
    public function setBuyerFeesAmount(?float $buyerFeesAmount): self
    {
        $this->buyerFeesAmount = $buyerFeesAmount;
        return $this;
    }



    /**
     * @return float
     */
    public function getVatAmount(): float
    {
        return $this->vatAmount;
    }

    /**
     * @param float $vatAmount
     * @return PropertyImmo
     */
    public function setVatAmount(float $vatAmount): self
    {
        $this->vatAmount = $vatAmount;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsSold(): bool
    {
        return $this->isSold;
    }

    /**
     * @param bool $isSold
     * @return PropertyImmo
     */
    public function setIsSold(bool $isSold): self
    {
        $this->isSold = $isSold;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getSoldDate(): ?\DateTime
    {
        return $this->soldDate;
    }

    /**
     * @param \DateTime|null $soldDate
     * @return PropertyImmo
     */
    public function setSoldDate(?\DateTime $soldDate): self
    {
        $this->soldDate = $soldDate;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getSoldPrice(): ?float
    {
        return $this->soldPrice;
    }

    /**
     * @param float|null $soldPrice
     * @return PropertyImmo
     */
    public function setSoldPrice(?float $soldPrice): self
    {
        $this->soldPrice = $soldPrice;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getMandateDate(): ?\DateTime
    {
        return $this->mandateDate;
    }

    /**
     * @param \DateTime|null $mandateDate
     * @return PropertyImmo
     */
    public function setMandateDate(?\DateTime $mandateDate): self
    {
        $this->mandateDate = $mandateDate;
        return $this;
    }


    /**
     * @return string
     */
    public function getMandateTypeName(): string
    {
        return $this->mandateTypeName;
    }

    /**
     * @param string $mandateTypeName
     * @return PropertyImmo
     */
    public function setMandateTypeName(string $mandateTypeName): self
    {
        $this->mandateTypeName = $mandateTypeName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionName(): string
    {
        return $this->transactionName;
    }

    /**
     * @param string $transactionName
     * @return PropertyImmo
     */
    public function setTransactionName(string $transactionName): self
    {
        $this->transactionName = $transactionName;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getRentIncreasedAmount(): ?float
    {
        return $this->rentIncreasedAmount;
    }

    /**
     * @param float|null $rentIncreasedAmount
     * @return PropertyImmo
     */
    public function setRentIncreasedAmount(?float $rentIncreasedAmount): self
    {
        $this->rentIncreasedAmount = $rentIncreasedAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return PropertyImmo
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPublicRef(): ?string
    {
        return $this->publicRef;
    }

    /**
     * @param string|null $publicRef
     * @return PropertyImmo
     */
    public function setPublicRef(?string $publicRef): self
    {
        $this->publicRef = $publicRef;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return PropertyImmo
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVirtualVisitUrl(): ?string
    {
        return $this->virtualVisitUrl;
    }

    /**
     * @param string|null $virtualVisitUrl
     * @return PropertyImmo
     */
    public function setVirtualVisitUrl(?string $virtualVisitUrl): self
    {
        $this->virtualVisitUrl = $virtualVisitUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return PropertyImmo
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    /**
     * @param string|null $address2
     * @return PropertyImmo
     */
    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * @param string|null $zipCode
     * @return PropertyImmo
     */
    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return PropertyImmo
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return PropertyImmo
     */
    public function setCountry(?string $country): self
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLat(): ?string
    {
        return $this->lat;
    }

    /**
     * @param string|null $lat
     * @return PropertyImmo
     */
    public function setLat(?string $lat): self
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLng(): ?string
    {
        return $this->lng;
    }

    /**
     * @param string|null $lng
     * @return PropertyImmo
     */
    public function setLng(?string $lng): self
    {
        $this->lng = $lng;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    /**
     * @param string|null $categoryName
     * @return PropertyImmo
     */
    public function setCategoryName(?string $categoryName): self
    {
        $this->categoryName = $categoryName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTypeName(): ?string
    {
        return $this->typeName;
    }

    /**
     * @param string|null $typeName
     * @return PropertyImmo
     */
    public function setTypeName(?string $typeName): self
    {
        $this->typeName = $typeName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubTypeName(): ?string
    {
        return $this->subTypeName;
    }

    /**
     * @param string|null $subTypeName
     * @return PropertyImmo
     */
    public function setSubTypeName(?string $subTypeName): self
    {
        $this->subTypeName = $subTypeName;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getFeatures(): ?array
    {
        return $this->features;
    }

    /**
     * @param array|null $features
     * @return PropertyImmo
     */
    public function setFeatures(?array $features): self
    {
        $this->features = $features;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getPictures(): ?array
    {
        return $this->pictures;
    }

    /**
     * @param array|null $pictures
     * @return PropertyImmo
     */
    public function setPictures(?array $pictures): self
    {
        $this->pictures = $pictures;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getTags(): ?array
    {
        return $this->tags;
    }

    /**
     * @param array|null $tags
     * @return PropertyImmo
     */
    public function setTags(?array $tags): self
    {
        $this->tags = $tags;
        return $this;
    }


    /**
     * @return bool|null
     */
    public function getPrestige(): ?bool
    {
        return $this->prestige;
    }

    /**
     * @param bool|null $prestige
     * @return PropertyImmo
     */
    public function setPrestige(?bool $prestige): self
    {
        $this->prestige = $prestige;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getLuxury(): ?bool
    {
        return $this->luxury;
    }

    /**
     * @param bool|null $luxury
     * @return PropertyImmo
     */
    public function setLuxury(?bool $luxury): self
    {
        $this->luxury = $luxury;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getNewProgram(): ?bool
    {
        return $this->newProgram;
    }

    /**
     * @param bool|null $newProgram
     * @return PropertyImmo
     */
    public function setNewProgram(?bool $newProgram): self
    {
        $this->newProgram = $newProgram;
        return $this;
    }

    /**
     * @return int
     */
    public function getArea(): int
    {
        return $this->area;
    }

    /**
     * @param int $area
     * @return PropertyImmo
     */
    public function setArea(int $area): self
    {
        $this->area = $area;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoomsNumber(): ?int
    {
        return $this->roomsNumber;
    }

    /**
     * @param int|null $roomsNumber
     * @return PropertyImmo
     */
    public function setRoomsNumber(?int $roomsNumber): self
    {
        $this->roomsNumber = $roomsNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBedroomsNumber(): ?int
    {
        return $this->bedroomsNumber;
    }

    /**
     * @param int|null $bedroomsNumber
     * @return PropertyImmo
     */
    public function setBedroomsNumber(?int $bedroomsNumber): self
    {
        $this->bedroomsNumber = $bedroomsNumber;
        return $this;
    }

    /**
     * @return array
     */
    public function getReportTo(): array
    {
        return $this->reportTo;
    }

    /**
     * @param array $reportTo
     * @return PropertyImmo
     */
    public function setReportTo(array $reportTo): self
    {
        $this->reportTo = $reportTo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return PropertyImmo
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return PropertyImmo
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }


}