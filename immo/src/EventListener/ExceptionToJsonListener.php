<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use LogicException;

class ExceptionToJsonListener
{

    public function __construct()
    {
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        if ($exception instanceof LogicException || $exception instanceof HttpException) {
            $statusCode = $exception instanceof HttpException ? $exception->getStatusCode() : 500;

            if ($exception->getPrevious()) $detail = $exception->getPrevious()->getMessage();
            else $detail = null;

            if($statusCode === 404) $detail = "Resource not found";

            $response = new JsonResponse([
                'data' => false,
                'error' => $exception->getMessage(),
                'message' => $detail
            ], $statusCode);

            $event->setResponse($response);
        }

//        if ($exception instanceof \TypeError) {
//            $message = 'Type error: ' . $exception->getMessage();
//            $apiException = new HttpException(400, $message, $exception);
//
//            $response = new JsonResponse([
//                "data" => false,
//                "error" => $apiException->getMessage()
//            ], $apiException->getStatusCode());
//
//            $event->setResponse($response);
//        }
    }
}







