#!/bin/sh

# Charger les variables du fichier .env
source .env

### --------- CREATE PROJECT --------- ##
symfony new --no-git api;


### ---------   MOVE FILES   --------- ##

# Move files from /api to /var/www/html/microservice
rm -f api/docker-compose.yml api/docker-compose.override.yml api/.env api/.env.test api/.gitignore && cp -r api/* /var/www/html/microservice && rm -rf api;

# Move command + service + listener + config from root of project to src
cp -rf Service /var/www/html/microservice/src && rm -rf Service
cp -f services.yaml /var/www/html/microservice/config && rm -f services.yaml
cp -rf EventListener /var/www/html/microservice/src && rm -rf EventListener


### --------- INSTALL BUNDLES --------- ##
composer install;

composer config extra.symfony.allow-contrib true

# Symfony/validator : pour valider les données avant l'enregistrement en bdd
composer require --no-interaction symfony/validator doctrine/annotations

#Nelmio bundle : pour la documentation
composer require --no-interaction twig asset
composer require --no-interaction nelmio/api-doc-bundle

#Mongodb bundle :	pour les micro services avec une BDD
composer require --no-interaction mongodb/mongodb
composer require --no-interaction doctrine/mongodb-odm-bundle



#Symfony/serializer : pour serializer/deserializer les données en json
composer require --no-interaction symfony/serializer

#symfony/property-access :	nécessaire pour utiliser le bundle Symfony/serializer
#	https://symfony.com/doc/current/components/property_access.html
composer require --no-interaction symfony/property-access

#symfony/http-client : pour faire des appels http
composer require --no-interaction symfony/http-client

composer require --no-interaction ama/symfony-mongo-maker-bundle:test-dev


chmod 755 config.sh
./config.sh

### --------- END INSTALL BUNDLES --------- ##

symfony server:start;
#while true; do sleep 1; done