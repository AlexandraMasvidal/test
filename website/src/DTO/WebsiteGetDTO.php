<?php

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes\Property;

class WebsiteGetDTO
{

    public function __construct(

    #[Groups(["read","update", "create"])]
    public readonly string|array|null $id = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $domain = null,

    #[Groups(["read","create","update"])]
    public readonly string|array|null $subdomain = null, 

    #[Groups(["read","create","update"])]
    public readonly \DateTime|array|null $createdAt = null, 

                  
    ) {}
}