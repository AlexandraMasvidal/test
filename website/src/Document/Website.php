<?php
namespace App\Document;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use OpenApi\Attributes\Property;

#[Document]

class Website {


    #[Id]
    #[Groups(["read","update", "create"])]
    private string $id;

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $domain; 

    #[Assert\NotBlank(message: "user is required", groups: ["create"])]
    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "collection" )]
    private array $user; 

    #[Groups(["read","update", "create"])]
    #[Field(type: "string" , nullable: true)]
    private ?string $subdomain; 

    #[Assert\NotBlank(message: "modules is required", groups: ["create"])]
    #[Property(type: "array[]")]
    #[Groups(["read","update", "create"])]
    #[Field(type: "collection" )]
    private array $modules; 

    #[Assert\NotBlank(message: "createdAt is required", groups: ["create"])]
    #[Groups(["read","update", "create"])]
    #[Field(type: "date" )]
    private \DateTime $createdAt; 


    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }

    /**
     * @param string|null $domain
     * @return Website
     */
    public function setDomain(?string $domain): self
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return array
     */
    public function getUser(): array
    {
        return $this->user;
    }

    /**
     * @param array $user
     * @return Website
     */
    public function setUser(array $user): self
    {
        $this->user = $user;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getSubdomain(): ?string
    {
        return $this->subdomain;
    }

    /**
     * @param string|null $subdomain
     * @return Website
     */
    public function setSubdomain(?string $subdomain): self
    {
        $this->subdomain = $subdomain;
        return $this;
    }

    /**
     * @return array
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    /**
     * @param array $modules
     * @return Website
     */
    public function setModules(array $modules): self
    {
        $this->modules = $modules;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Website
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }


}